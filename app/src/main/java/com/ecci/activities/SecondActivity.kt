package com.ecci.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        getFromSharedPreferences()
        failButton.setOnClickListener {
            setResult(-5)
            finish()
        }
        okButton.setOnClickListener {
            setResult(RESULT_OK)
            finish()
        }
    }

    //leer en SharedPreferences
    private fun getFromSharedPreferences() {
        val preferences = getSharedPreferences("myapp", MODE_PRIVATE)
        findViewById<TextView>(R.id.textView).text = preferences.getString("nameUser", "")
    }
}