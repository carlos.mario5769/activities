package com.ecci.activities

import android.content.Intent
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    val REQUEST_CODE_SECOND = 13
    val REQUEST_CODE_PICKER_IMAGE = 15

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        saveButton.setOnClickListener {
            saveSharedPreferences()
            goToNext()
        }
        continueButton.setOnClickListener {
            goToNext()
        }
        pickButton.setOnClickListener {
            pickFromGallery()
        }
    }

    private fun goToNext() {
        val intent = Intent(this, SecondActivity::class.java)
        startActivityForResult(intent, REQUEST_CODE_SECOND)
    }

    //guardar en SharedPreferences
    private fun saveSharedPreferences() {
        val preferences = getSharedPreferences("myapp", MODE_PRIVATE)
        val editor = preferences.edit()
        editor.putString("nameUser", editText.text.toString())
        editor.commit()
    }

    //Abrir la galería
    private fun pickFromGallery() {
        //Quiero abrir una aplicación que me permita elegir un archivo
        val intent = Intent(Intent.ACTION_PICK)
        //Restringir solo a imagenes
        intent.type = "image/*"
        startActivityForResult(intent, REQUEST_CODE_PICKER_IMAGE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == REQUEST_CODE_SECOND) {
            when (resultCode) {
                RESULT_OK -> Toast.makeText(this, "El resultado fue exitoso", Toast.LENGTH_SHORT)
                    .show()
                -5 -> Toast.makeText(this, "El resultado fue fallido", Toast.LENGTH_SHORT).show()
                else -> Toast.makeText(this, "El resultado es incierto", Toast.LENGTH_SHORT).show()
            }
        }
        if(requestCode == REQUEST_CODE_PICKER_IMAGE) {
            if(resultCode == RESULT_OK) {
                val uri = data?.data
                imageView.setImageURI(uri)
            }
            else {
                Toast.makeText(this, "No se seleccionó imagen", Toast.LENGTH_SHORT).show()
            }
        }
    }
}